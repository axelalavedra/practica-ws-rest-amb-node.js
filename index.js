var express = require('express'), app = express();
var db = require('./db.js');
var bodyParser = require('body-parser');


app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.get('/', function(req,res) {

res.send("Hola mon!");

});


app.get('/apps', function(req,res){

	db.query("SELECT * FROM app", function(err,result){
		if(!err){
			res.json(result.rows);
		}
		else {
			res.send("Error a la DB");
			console.log(err);
		}
	});
});

app.get('/apps/:code', function(req,res){

	db.query("SELECT * FROM record where app_code=$1",[req.params.code], function(err,result){
		if(result.rows.length>0){
			res.json(result.rows);
		}
		else {
			res.status(404);
			res.end("Error 404: App not found");
			console.log(err);
		}
	});
});

app.post('/apps/:code', function(req,res){
	if(!req.body.player || !req.body.score){
			res.status(422);
			res.end("Error 422: no player nor score found")
	}else{
		db.query("INSERT INTO record(app_code,player,score) VALUES ($1,$2,$3)",[req.params.code,req.body.player,req.body.score], function(err,result){	
		if(err){
			res.status(500);
			res.end("Error 500: server error");
			console.log(err);
		}
		else{
			res.send("Correct insertion");
		}
		
	});
	}
	
});

app.listen(process.env.PORT || 3000, function(){
	console.log("Listening Port 3000");
});
